package com.spell;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.spell.vo.SpellCheckResponse;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class SpellCheckAndSuggestionApplicationTests {

	@Autowired
	private TestRestTemplate testTemplate;
	
	@Test
	public void testWordWithNoVowels() {
		ResponseEntity<SpellCheckResponse> body = this.testTemplate.getForEntity("/spelling/blln",SpellCheckResponse.class);
		assertThat(body.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testWordWithRepeatedCharacters() {
		ResponseEntity<SpellCheckResponse> body = this.testTemplate.getForEntity("/spelling/ballloon",SpellCheckResponse.class);
		assertThat(body.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(body.getBody().isCorrect()).isEqualTo(false);
		//assertThat(body.getBody()).e
	}
	
	@Test
	public void testWordUpperCaseLetters() {
		ResponseEntity<SpellCheckResponse> body = this.testTemplate.getForEntity("/spelling/BallooN",SpellCheckResponse.class);
		assertThat(body.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(body.getBody().isCorrect()).isEqualTo(false);
		//assertThat(body.getBody()).e
	}
	
	@Test
	public void testWordStartingWithCapital() {
		ResponseEntity<SpellCheckResponse> body = this.testTemplate.getForEntity("/spelling/Balloon",SpellCheckResponse.class);
		assertThat(body.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
		assertThat(body.getBody().isCorrect()).isEqualTo(true);
		//assertThat(body.getBody()).e
	}

}
