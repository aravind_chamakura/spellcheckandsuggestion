package com.spell.services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spell.utils.SpellCorrectionAndSuggestion;
import com.spell.vo.SpellCheckResponse;

/*
 * Implementation class for SpellCheckService
 */
@Service
public class SpellCheckServiceImpl implements SpellCheckService {

	Logger LOGGER = LoggerFactory.getLogger(SpellCheckServiceImpl.class);
	
	@Autowired
	SpellCorrectionAndSuggestion spellCorrection;

	private static final Pattern REPEATED_PATTERN = Pattern
			.compile("(\\w)\\1{2,}");
	private static final Pattern CAPITAL_LETTER_PATTERN = Pattern
			.compile("^(.[A-Z a-z])(?=.*[a-z])(?=.*[A-Z])");
	private static final Pattern VOWEL_PATTERN = Pattern.compile("[aeiou]");

	/*
	 * Check if the given word matches the list of regular expressions. If it
	 * does not match, then word is incorrect and spell suggestion is invoked.
	 * If the word is correct spell suggestion is invoked to check if word
	 * exists in dictionary and suggestion are made.
	 * 
	 * @param String word
	 * 
	 * @return SpellCheckResponse object containing the suggestions
	 */
	@Override
	public SpellCheckResponse checkSpellingAndSuggest(String word) {
		LOGGER.info("Service call for check spell with word " + word);
		SpellCheckResponse spellCheckResponse = new SpellCheckResponse();
		boolean inValidWord = matchPattern(REPEATED_PATTERN, word)
				|| matchPattern(CAPITAL_LETTER_PATTERN, word)
				|| !matchPattern(VOWEL_PATTERN, word);
		if (inValidWord) {
			LOGGER.trace("Word did not match atleast one of the Regex Patter");
			spellCheckResponse.setCorrect(false);
			spellCorrection.checkSpelling(word.toLowerCase(),
					spellCheckResponse);
		} else {
			spellCheckResponse.setCorrect(true);
			spellCorrection.checkSpelling(word.toLowerCase(),
					spellCheckResponse);

		}
		LOGGER.info("Returning response");
		return spellCheckResponse;
	}

	/*
	 * Matches the regex to the given word.
	 */
	private boolean matchPattern(Pattern pattern, String word) {
		Matcher matcher = pattern.matcher(word);
		return matcher.find();

	}

}
