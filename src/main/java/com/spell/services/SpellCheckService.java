package com.spell.services;

import com.spell.vo.SpellCheckResponse;


public interface SpellCheckService {

	public SpellCheckResponse checkSpellingAndSuggest(String word);
}

