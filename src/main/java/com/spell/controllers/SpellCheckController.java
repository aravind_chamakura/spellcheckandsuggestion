package com.spell.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spell.services.SpellCheckService;
import com.spell.vo.SpellCheckResponse;

/*
 * REST controller for handling  spelling service
 * 
 */
@RestController
public class SpellCheckController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SpellCheckController.class);

	@Autowired
	SpellCheckService spellCheckService;

	@RequestMapping(value = "/spelling/{word}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<SpellCheckResponse> getSpellingSuggestion(
			@PathVariable("word") String word) {
		LOGGER.info("Received request for /spelling for word: " + word);
		SpellCheckResponse spellResponse = spellCheckService
				.checkSpellingAndSuggest(word);
		if (spellResponse.getSuggestions().isEmpty()) {
			LOGGER.info("No Suggestions found, Returning a 404 Error");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		StringBuffer sb = new StringBuffer(
				"Suggesions are found for the word: ").append(word)
				.append(" are ")
				.append(spellResponse.getSuggestions().toString());
		LOGGER.info(sb.toString());
		return ResponseEntity.status(HttpStatus.OK).body(spellResponse);
	}
}
