package com.spell.utils;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Trie {
	
	Logger LOGGER = LoggerFactory.getLogger(Trie.class);

	private class TrieNode {
		Map<Character,TrieNode> child;
		String str;
		boolean isEndOfWord;
		public TrieNode(){
			child = new TreeMap<Character, TrieNode>();
			isEndOfWord = false;
		}
		@Override
		public String toString() {
			Set<Character> set = child.keySet();
			
			return "TrieNode [child=" + child + ", isEndOfWord=" + str
					+ " " + set.toString() + "]";
		}
		
	}
	
	private final TrieNode root;
	public Trie(){
		root = new TrieNode();
	}
	
	/*
	 * Insert word in to the Trie data structure.
	 */
	public void insertWord(String word){
		LOGGER.debug(String.format("Inserting Word (%s) into data structure", word));
		TrieNode current = root;
		for(int i=0; i<word.length(); i++){
			char ch = word.charAt(i);
			TrieNode node = current.child.get(ch);
			if(node ==null){
				node = new TrieNode();
				current.child.put(ch, node);
			}
			current = node;
		}
		current.isEndOfWord = true;
		current.str = word;
	}
	
	
	public boolean searchRecursive(String word){
		LOGGER.debug("Searching the Data structure for the word " + word);
		boolean isFound = false;
		isFound = searchRecursive(root,word,0);
		LOGGER.debug("Is word in the dictionary " + isFound);
		return isFound;

	}
	private boolean searchRecursive(TrieNode current, String word, int index){
		if(index == word.length()){
			return current.isEndOfWord;
		}
		TrieNode node = current.child.get(word.charAt(index));
		if(node == null){
			return false;
		}
		current = node;
		
		return searchRecursive(node,word,++index);
	}
	
	/*
	 * Process single character edit (delete,transpose,replace,insert) for the word.
	 * 
	 * Return a set of words
	 */
	public Set<String>  editOne(String word){
		final String letters = "abcefghijklmnopqrstuvwxyz";
		Map<String,String> splits = new LinkedHashMap<String,String>();
		for(int i=0;i<word.length()+1;i++){
			splits.put(word.substring(0,i), word.substring(i));
		}
		Set<String> wordSet = new HashSet<String>();
		//Process Single Deletes
		splits.forEach((k,v)->{
			if(v != null && v.length() >=1)
			wordSet.add(k + v.substring(1));
			
		});
		//process single transpose
		splits.forEach((k,v)->{
			if(v != null && v.length() >=2) {
			wordSet.add(k + v.charAt(1)+ v.charAt(0) + v.substring(2) );
			}
			
		});
		//process single replace
		splits.forEach((k,v) -> {
			if(v!= null && v.length() >=1)
			for(int i =0; i< letters.length(); i++){
				wordSet.add(k + letters.charAt(i) + v.substring(1));
			}
		});
		
		//process single inserts
		splits.forEach((k,v) -> {
			for(int i =0; i< letters.length(); i++){
				wordSet.add(k + letters.charAt(i) + v);
			}

		});
		return wordSet;
	}

	
}
