package com.spell.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.spell.vo.SpellCheckResponse;

@Component
public class SpellCorrectionAndSuggestion {

	Logger LOGGER = LoggerFactory.getLogger(SpellCorrectionAndSuggestion.class);
	private Trie trie;
	
    @Value("${spellcheck.wordlist.fileName}")
	private String fileName;

	/*
	 * Load the words from the file and create a Trie Data structure
	 */
	@PostConstruct
	public void readAndPolulate() throws Exception {
		LOGGER.info("Reading words File and Loading into Data Structure");
		trie = new Trie();
		String str = "";
		try {
			InputStream input = this.getClass().getResourceAsStream(
					fileName);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					input));
			if (input != null) {
				while ((str = reader.readLine()) != null) {
					// System.out.println(str);
					trie.insertWord(str);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error reading the words File, Aborting the Server startup. Please make sure /static/words.txt file is available");
			throw e;
		}
		LOGGER.info("Completed Loading the file into data structure");
	}

	/*
	 * Check if the given word exists, if yes return the word. Otherwise try
	 * single edit and search If found during single edit, return the words as
	 * suggessions.
	 */
	public void checkSpelling(String word, SpellCheckResponse spellCheckResponse) {
		LOGGER.info("Checking for spelling in the Trie Data Structure");
		boolean isFound = trie.searchRecursive(word);
		List<String> suggestions = new ArrayList<String>();

		if (isFound) {
			LOGGER.debug("Input word is found with no edits");
			suggestions.add(word);
		} else {
			LOGGER.debug("Input word is not found, tring by making edits to word");
			spellCheckResponse.isCorrect();
			Set<String> editedWords = trie.editOne(word);
			LOGGER.debug("List of words after edit " +  editedWords.toString() );
			editedWords.forEach((k) -> {
				if (trie.searchRecursive(k))
					suggestions.add(k);
			});
		}
		spellCheckResponse.setSuggestions(suggestions);
	}
}
