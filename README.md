# README #


### Spell Check and Suggestion ###

* Microservice to check word spelling and provide suggestions
* 1.0

### How do I get set up? ###

*  Use git clone to download the code. git clone https://aravind_chamakura@bitbucket.org/aravind_chamakura/spellcheckandsuggestion.git
*  Go into the directory and Run mvn package
*  Run java -jar target/SpellCheckAndSuggestion-1.0.jar

Point to http:localhost:8080/spelling/{your word}  in a browser or PostMan or favorite REST utility